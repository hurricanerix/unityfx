﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(UnityFXSource))]
public class UnityFXSourceEditor : Editor
{


    public override void OnInspectorGUI()
    {
        UnityFXSource t = (target as UnityFXSource);
        t.Init();

        if (GUILayout.Button("Default"))
        {
            t.Clip.Reset();
            t.Play();
        }


        if (GUILayout.Button("Pickup/Coin"))
        {
            UnityFXHelper.LoadPreset(UnityFXHelper.Presets.PICKUP_COIN, t.Clip);
            t.Play();
        }

        if (GUILayout.Button("Laser/Shoot"))
        {
            UnityFXHelper.LoadPreset(UnityFXHelper.Presets.LASER_SHOOT, t.Clip);
            t.Play();
        }

        if (GUILayout.Button("Explosion"))
        {
            UnityFXHelper.LoadPreset(UnityFXHelper.Presets.EXPLOSION, t.Clip);
            t.Play();
        }

        if (GUILayout.Button("Hit/Hurt"))
        {
            UnityFXHelper.LoadPreset(UnityFXHelper.Presets.HIT_HURT, t.Clip);
            t.Play();
        }

        if (GUILayout.Button("Jump"))
        {
            UnityFXHelper.LoadPreset(UnityFXHelper.Presets.JUMP, t.Clip);
            t.Play();
        }

        if (GUILayout.Button("Blip/Select"))
        {
            UnityFXHelper.LoadPreset(UnityFXHelper.Presets.BLIP_SELECT, t.Clip);
            t.Play();
        }

        EditorGUI.BeginChangeCheck();
        int waveType = EditorGUILayout.Popup("wave type", (int)t.Clip.waveType, new string[] { "Square", "Sawtooth", "Sine", "Noise" });
        if (EditorGUI.EndChangeCheck())
        {
            Undo.RecordObject(target, "Change wave type");
            t.Clip.waveType = (UnityFX.WaveTypes)waveType;
        }

        EditorGUI.BeginChangeCheck();
        float baseFreq = EditorGUILayout.Slider("BASE FREQ", t.clip.baseFreq, 0f, 1f);
        if (EditorGUI.EndChangeCheck())
        {
            Undo.RecordObject(target, "Changed BASE FREQ");
            t.clip.baseFreq = baseFreq;
        }

        Slider(target, "freqLimit", ref t.Clip.freqLimit);
        Slider(target, "freqRamp", ref t.Clip.freqRamp);
        Slider(target, "freqDramp", ref t.Clip.freqDramp);
        Slider(target, "duty", ref t.Clip.duty);

        Slider(target, "dutyRamp", ref t.Clip.dutyRamp);

        Slider(target, "vibStrength", ref t.Clip.vibStrength);
        Slider(target, "vibSpeed", ref t.Clip.vibSpeed);
        Slider(target, "vibDelay", ref t.Clip.vibDelay);

        Slider(target, "envAttack", ref t.Clip.envAttack);
        Slider(target, "envSustain", ref t.Clip.envSustain);
        Slider(target, "envDecay", ref t.Clip.envDecay);
        Slider(target, "envPunch", ref t.Clip.envPunch);

        Slider(target, "lpfResonance", ref t.Clip.lpfResonance);
        Slider(target, "lpfFreq", ref t.Clip.lpfFreq);
        Slider(target, "lpfRamp", ref t.Clip.lpfRamp);
        Slider(target, "hpfFreq", ref t.Clip.hpfFreq);
        Slider(target, "hpfRamp", ref t.Clip.hpfRamp);

        Slider(target, "phaOffset", ref t.Clip.phaOffset);
        Slider(target, "phaRamp", ref t.Clip.phaRamp);

        Slider(target, "repeatSpeed", ref t.Clip.repeatSpeed);

        Slider(target, "arpSpeed", ref t.Clip.arpSpeed);
        Slider(target, "arpMod", ref t.Clip.arpMod);

        if (GUILayout.Button("Play"))
        {
            t.Play();
        }
    }

    private void Slider(UnityEngine.Object target, string label, ref float value)
    {
        Slider(target, label, ref value, 0f, 1f);
    }

    private void Slider(UnityEngine.Object target, string label, ref float value, float minValue, float maxValue)
    {
        EditorGUI.BeginChangeCheck();
        float duty = EditorGUILayout.Slider(label, value, minValue, maxValue);
        if (EditorGUI.EndChangeCheck())
        {
            Undo.RecordObject(target, "Changed duty");
            value = duty;
        }
    }
}