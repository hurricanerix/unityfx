﻿using UnityEngine;

public class UnityFXSource : MonoBehaviour
{
    public UnityFXClip Clip
    {
        get { return this.clip; }
        set { this.clip = value; }
    }

    public UnityFX FX
    {
        get { return this.fx; }
        set { this.fx = value; }
    }

    public void Play()
    {
        this.audioSource.Play();
    }

    private AudioSource audioSource;
    private UnityFX fx;
    public UnityFXClip clip;

    private void Start()
    {
        Init();
    }

    public void Init()
    {
        if (this.audioSource == null)
        {
            this.audioSource = GetComponent<AudioSource>();
            if (this.audioSource == null)
            {
                this.audioSource = gameObject.AddComponent<AudioSource>();
            }
        }

        if (this.clip == null)
        {
            this.clip = ScriptableObject.CreateInstance<UnityFXClip>();
            UnityFXHelper.LoadPreset(UnityFXHelper.Presets.DEFAULT, this.clip);
        }

        if (this.fx == null)
        {
            this.fx = new UnityFX();
            this.fx.SetClip(this.clip);
            this.audioSource.clip = fx.GetAudioClip();
        }
    }
}