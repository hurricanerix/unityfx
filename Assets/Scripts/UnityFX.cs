﻿using UnityEngine;

/// <summary>
/// 
/// </summary>
public class UnityFX
{
    public const Modes DefaultMode = Modes.REALTIME;
    public const SampleRates DefaultSampleRate = SampleRates._44100_HZ;
    public const float DefaultVolume = 0.5f;

    /// <summary>
    /// Supported modes:
    /// REALTIME - waveform data is generated on every play.
    /// LAZY - waveform data is generated the first time it is requested
    ///        then cached for future calls.
    /// INIT - waveform data is generated for all components on init.
    /// BAKE - waveform data is generated at build time and linked to
    ///        the objects audio source via a audio clip component.
    /// </summary>
    public enum Modes { REALTIME, LAZY, INIT, BAKE };

    /// <summary>
    /// 
    /// </summary>
    public enum SampleRates { _44100_HZ, _22050_HZ };

    public enum WaveTypes { SQAURE = 0, SAWTOOTH = 1, SINE = 2, NOISE = 3 }

    /// <summary>
    /// 
    /// </summary>
    public static UnityFX.Modes mode = DefaultMode;

    /// <summary>
    /// 
    /// </summary>
    public static UnityFX.SampleRates sampleRate = DefaultSampleRate;

    /// <summary>
    /// 
    /// </summary>
    public static float volume = DefaultVolume;

    public UnityFX()
    {
        this.SetClip(ScriptableObject.CreateInstance<UnityFXClip>());
        this.Reset();
    }

    public UnityFX(UnityFXClip clip)
    {
        this.SetClip(clip);
        this.Reset();
    }

    public void Reset()
    {
        this.position = 0;

        switch (UnityFX.sampleRate)
        {
            case UnityFX.SampleRates._22050_HZ:
                this.clipSampleRate = 22050;
                break;
            case UnityFX.SampleRates._44100_HZ:
            // Fallthrough to default case
            default:
                this.clipSampleRate = 44100;
                break;
        }

        this.clipFreq = 440;
    }

    public void SetClip(UnityFXClip clip)
    {
        this.clip = clip;
        this.ResetSample(true);
    }

    public AudioClip GetAudioClip()
    {
        Debug.Log("GetAudioClip()");
        this.Reset();

        string name = "UnityFX";
        int lengthSamples = this.clipSampleRate * 2; // Number of sample frames.
        int channels = 1; // channels	Number of channels per frame.
        int frequency = this.clipSampleRate; //	Sample frequency of clip.
        bool stream = true; // True if clip is streamed, that is if the pcmreadercallback generates data on the fly.

        Debug.Log("Name: " + name);
        Debug.Log("LengthSamples: " + lengthSamples);
        Debug.Log("Channels: " + channels);
        Debug.Log("Frequency: " + frequency);
        Debug.Log("Stream: " + stream);

        return AudioClip.Create(name, lengthSamples, channels, frequency, stream, this.OnAudioRead, this.OnAudioSetPosition);
    }

    float master_vol = 0.05f;

    bool playing_sample = false;
    int phase;
    float fperiod; // double
    float fmaxperiod; // double
    float fslide; // double
    float fdslide; // double
    int period;
    float square_duty;
    float square_slide;
    int env_stage;
    int env_time;
    int[] env_length; // 3
    float env_vol;
    float fphase;
    float fdphase;
    int iphase;
    float[] phaser_buffer; // 1024
    int ipp;
    float[] noise_buffer; // 32
    float fltp;
    float fltdp;
    float fltw;
    float fltw_d;
    float fltdmp;
    float fltphp;
    float flthp;
    float flthp_d;
    float vib_phase;
    float vib_speed;
    float vib_amp;
    int rep_time;
    int rep_limit;
    int arp_time;
    int arp_limit;
    float arp_mod; // double

    float vselected; // pointer
    int vcurbutton = -1;

    int wav_bits = 16;
    int wav_freq = 44100;

    int file_sampleswritten;
    float filesample = 0.0f;
    int fileacc = 0;

    private UnityFXClip clip;
    private int position;

    private int clipSampleRate;
    private int clipFreq;

    void ResetSample(bool restart)
    {
        Debug.Log("ResetSample");
        position = 0;
        env_length = new int[3];
        phaser_buffer = new float[1024];
        noise_buffer = new float[32];

        if (!restart)
            phase = 0;
        fperiod = 100.0f / (this.clip.baseFreq * this.clip.baseFreq + 0.001f);
        period = (int)fperiod;
        fmaxperiod = 100.0f / (this.clip.freqLimit * this.clip.freqLimit + 0.001f);
        fslide = 1.0f - Mathf.Pow(this.clip.freqRamp, 3.0f) * 0.01f;
        fdslide = -Mathf.Pow(this.clip.freqDramp, 3.0f) * 0.000001f;
        square_duty = 0.5f - this.clip.duty * 0.5f;
        square_slide = -this.clip.dutyRamp * 0.00005f;
        if (this.clip.arpMod >= 0.0f)
            arp_mod = 1.0f - Mathf.Pow(this.clip.arpMod, 2.0f) * 0.9f;
        else
            arp_mod = 1.0f + Mathf.Pow(this.clip.arpMod, 2.0f) * 10.0f;
        arp_time = 0;
        arp_limit = (int)(Mathf.Pow(1.0f - this.clip.arpSpeed, 2.0f) * 20000 + 32);
        if (this.clip.arpSpeed == 1.0f)
            arp_limit = 0;
        if (!restart)
        {
            // reset filter
            fltp = 0.0f;
            fltdp = 0.0f;
            fltw = Mathf.Pow(this.clip.lpfFreq, 3.0f) * 0.1f;
            fltw_d = 1.0f + this.clip.lpfRamp * 0.0001f;
            fltdmp = 5.0f / (1.0f + Mathf.Pow(this.clip.lpfResonance, 2.0f) * 20.0f) * (0.01f + fltw);
            if (fltdmp > 0.8f) fltdmp = 0.8f;
            fltphp = 0.0f;
            flthp = Mathf.Pow(this.clip.hpfFreq, 2.0f) * 0.1f;
            flthp_d = 1.0f + this.clip.hpfRamp * 0.0003f;
            // reset vibrato
            vib_phase = 0.0f;
            vib_speed = Mathf.Pow(this.clip.vibSpeed, 2.0f) * 0.01f;
            vib_amp = this.clip.vibStrength * 0.5f;
            // reset envelope
            env_vol = 0.0f;
            env_stage = 0;
            env_time = 0;
            env_length[0] = (int)(this.clip.envAttack * this.clip.envAttack * 100000.0f);
            env_length[1] = (int)(this.clip.envSustain * this.clip.envSustain * 100000.0f);
            env_length[2] = (int)(this.clip.envDecay * this.clip.envDecay * 100000.0f);

            fphase = Mathf.Pow(this.clip.phaOffset, 2.0f) * 1020.0f;
            if (this.clip.phaOffset < 0.0f) fphase = -fphase;
            fdphase = Mathf.Pow(this.clip.phaRamp, 2.0f) * 1.0f;
            if (this.clip.phaRamp < 0.0f) fdphase = -fdphase;
            iphase = Mathf.Abs((int)fphase);
            ipp = 0;
            for (int i = 0; i < 1024; i++)
                phaser_buffer[i] = 0.0f;

            for (int i = 0; i < 32; i++)
                noise_buffer[i] = Random.Range(-1f, 1f);

            rep_time = 0;
            rep_limit = (int)(Mathf.Pow(1.0f - this.clip.repeatSpeed, 2.0f) * 20000 + 32);
            if (this.clip.repeatSpeed == 0.0f)
                rep_limit = 0;
        }
    }

    /// <summary>
    /// This callback is invoked whenever the clip loops or changes playback position.
    /// </summary>
    /// <param name="pos"></param>
    private void OnAudioSetPosition(int position)
    {
        Debug.Log("OnAudioSetPosition(" + position + ")");
        this.ResetSample(!(position == 0));
        this.position = position;
        playing_sample = true;
    }

    /// <summary>
    /// This callback is invoked to generate a block of sample data. Non-streamed clips call this
    /// only once at creation time while streamed clips call this continuously.
    /// </summary>
    /// <param name="data"></param>
    private void OnAudioRead(float[] data)
    {
        Debug.Log("OnAudioRead(data[" + data.Length + "])");

        for (int i = 0; i < data.Length; i++)
        {
            if (!playing_sample)
            {
                data[i] = 0;
                continue;
            }

            this.position++;

            // data[i] = Mathf.Sin(2 * Mathf.PI * this.clipFreq * position / this.clipSampleRate);
            // continue;

            rep_time++;
            if (rep_limit != 0 && rep_time >= rep_limit)
            {
                rep_time = 0;
                ResetSample(true);
            }

            // frequency envelopes/arpeggios
            arp_time++;
            if (arp_limit != 0 && arp_time >= arp_limit)
            {
                arp_limit = 0;
                fperiod *= arp_mod;
            }
            fslide += fdslide;
            fperiod *= fslide;
            if (fperiod > fmaxperiod)
            {
                fperiod = fmaxperiod;
                if (this.clip.freqLimit > 0.0f)
                    playing_sample = false;
            }
            float rfperiod = fperiod;
            if (vib_amp > 0.0f)
            {
                vib_phase += vib_speed;
                rfperiod = fperiod * (1.0f + Mathf.Sin(vib_phase) * vib_amp);
            }
            period = (int)rfperiod;
            if (period < 8) period = 8;
            square_duty += square_slide;
            if (square_duty < 0.0f) square_duty = 0.0f;
            if (square_duty > 0.5f) square_duty = 0.5f;
            // volume envelope
            env_time++;
            if (env_time > env_length[env_stage])
            {
                env_time = 0;
                env_stage++;
                if (env_stage == 3)
                {
                    playing_sample = false;
                    continue;
                }
            }
            if (env_stage == 0)
                env_vol = (float)env_time / env_length[0];
            if (env_stage == 1)
                env_vol = 1.0f + Mathf.Pow(1.0f - (float)env_time / env_length[1], 1.0f) * 2.0f * this.clip.envPunch;
            if (env_stage == 2)
                env_vol = 1.0f - (float)env_time / env_length[2];

            // phaser step
            fphase += fdphase;
            iphase = Mathf.Abs((int)fphase);
            if (iphase > 1023) iphase = 1023;

            if (flthp_d != 0.0f)
            {
                flthp *= flthp_d;
                if (flthp < 0.00001f) flthp = 0.00001f;
                if (flthp > 0.1f) flthp = 0.1f;
            }

            float ssample = 0.0f;
            for (int si = 0; si < 8; si++) // 8x supersampling
            {
                float sample = 0.0f;
                phase++;
                if (phase >= period)
                {
                    //				phase=0;
                    phase %= period;
                    if (this.clip.waveType == WaveTypes.NOISE)
                        for (int j = 0; j < 32; j++)
                            noise_buffer[j] = Random.Range(-1f, 1f);
                }
                // base waveform
                float fp = (float)phase / period;
                switch (this.clip.waveType)
                {
                    case WaveTypes.SQAURE:
                        if (fp < square_duty)
                            sample = 0.5f;
                        else
                            sample = -0.5f;
                        break;
                    case WaveTypes.SAWTOOTH:
                        sample = 1.0f - fp * 2f;
                        break;
                    case WaveTypes.SINE:
                        sample = (float)Mathf.Sin(fp * 2f * Mathf.PI);
                        break;
                    case WaveTypes.NOISE:
                        sample = noise_buffer[phase * 32 / period];
                        break;
                }
                // lp filter
                float pp = fltp;
                fltw *= fltw_d;
                if (fltw < 0.0f) fltw = 0.0f;
                if (fltw > 0.1f) fltw = 0.1f;
                if (this.clip.lpfFreq != 1.0f)
                {
                    fltdp += (sample - fltp) * fltw;
                    fltdp -= fltdp * fltdmp;
                }
                else
                {
                    fltp = sample;
                    fltdp = 0.0f;
                }
                fltp += fltdp;
                // hp filter
                fltphp += fltp - pp;
                fltphp -= fltphp * flthp;
                sample = fltphp;
                // phaser
                phaser_buffer[ipp & 1023] = sample;
                sample += phaser_buffer[(ipp - iphase + 1024) & 1023];
                ipp = (ipp + 1) & 1023;
                // final accumulation and envelope application
                ssample += sample * env_vol;
            }
            ssample = ssample / 8 * master_vol;

            ssample *= 2.0f * UnityFX.volume;

            if (ssample > 1.0f) ssample = 1.0f;
            if (ssample < -1.0f) ssample = -1.0f;

            data[i] = ssample;
        }
    }
}

// using UnityEngine;

// [HelpURL("http://example.com/docs/MyComponent.html")]
// public class UnityFX : MonoBehaviour
// {
//     public int Mode
//     {
//         get { return (int)this.mode; }
//         set
//         {
//             this.mode = (UnityFXCore.Modes)value;
//             UnityFXCore.mode = this.mode;
//         }
//     }

//     public int SampleRate
//     {
//         get { return (int)this.sampleRate; }
//         set
//         {
//             this.sampleRate = (UnityFXCore.SampleRates)value;
//             UnityFXCore.sampleRate = this.sampleRate;
//         }
//     }


//     public float Volume
//     {
//         get { return this.volume; }
//         set
//         {
//             this.volume = value;
//             UnityFXCore.volume = this.volume;
//         }
//     }

//     [SerializeField]
//     private UnityFXCore.Modes mode = UnityFXCore.DefaultMode;

//     [SerializeField]
//     private UnityFXCore.SampleRates sampleRate = UnityFXCore.DefaultSampleRate;

//     [SerializeField]
//     private float volume = UnityFXCore.DefaultVolume;

//     private void Reset()
//     {
//         this.Mode = (int)UnityFXCore.DefaultMode;
//         this.SampleRate = (int)UnityFXCore.DefaultSampleRate;
//         this.Volume = UnityFXCore.DefaultVolume;
//     }
// }

