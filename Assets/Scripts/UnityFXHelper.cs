﻿using UnityEngine;

public static class UnityFXHelper
{
    public enum Presets
    {
        DEFAULT,
        PICKUP_COIN,
        LASER_SHOOT,
        EXPLOSION,
        POWERUP,
        HIT_HURT,
        JUMP,
        BLIP_SELECT,
    };

    public static void LoadPreset(Presets preset, UnityFXClip clip)
    {
        clip.Reset();
        switch (preset)
        {
            case Presets.PICKUP_COIN:
                clip.baseFreq = 0.4f + Random.Range(0f, 0.5f);
                clip.envAttack = 0.0f;
                clip.envSustain = Random.Range(0f, 0.1f);
                clip.envDecay = 0.1f + Random.Range(0f, 0.4f);
                clip.envPunch = 0.3f + Random.Range(0f, 0.3f);
                if (Random.Range(0, 1) == 1)
                {
                    clip.arpSpeed = 0.5f + Random.Range(0f, 0.2f);
                    clip.arpMod = 0.2f + Random.Range(0f, 0.4f);
                }
                break;
            case Presets.LASER_SHOOT:
                clip.waveType = (UnityFX.WaveTypes)Random.Range(0, 2);
                if (clip.waveType == UnityFX.WaveTypes.SINE && Random.Range(0, 1) == 1)
                    clip.waveType = (UnityFX.WaveTypes)Random.Range(0, 1);
                clip.baseFreq = 0.5f + Random.Range(0f, 0.5f);
                clip.freqLimit = clip.baseFreq - 0.2f - Random.Range(0f, 0.6f);
                if (clip.freqLimit < 0.2f)
                    clip.freqLimit = 0.2f;
                clip.freqRamp = -0.15f - Random.Range(0f, 0.2f);
                if (Random.Range(0, 2) == 0)
                {
                    clip.baseFreq = 0.3f + Random.Range(0f, 0.6f);
                    clip.freqLimit = Random.Range(0f, 0.1f);
                    clip.freqRamp = -0.35f - Random.Range(0f, 0.3f);
                }
                if (Random.Range(0, 1) == 1)
                {
                    clip.duty = Random.Range(0f, 0.5f);
                    clip.dutyRamp = Random.Range(0f, 0.2f);
                }
                else
                {
                    clip.duty = 0.4f + Random.Range(0f, 0.5f);
                    clip.dutyRamp = -Random.Range(0f, 0.7f);
                }
                clip.envAttack = 0.0f;
                clip.envSustain = 0.1f + Random.Range(0f, 0.2f);
                clip.envDecay = Random.Range(0f, 0.4f);
                if (Random.Range(0, 1) == 1)
                    clip.envPunch = Random.Range(0f, 0.3f);
                if (Random.Range(0, 2) == 0)
                {
                    clip.phaOffset = Random.Range(0f, 0.2f);
                    clip.phaRamp = -Random.Range(0f, 0.2f);
                }
                if (Random.Range(0, 1) == 1)
                    clip.hpfFreq = Random.Range(0f, 0.3f);
                break;
            case Presets.EXPLOSION:
                clip.waveType = UnityFX.WaveTypes.NOISE;
                if (Random.Range(0, 1) == 1)
                {
                    clip.baseFreq = 0.1f + Random.Range(0f, 0.4f);
                    clip.freqRamp = -0.1f + Random.Range(0f, 0.4f);
                }
                else
                {
                    clip.baseFreq = 0.2f + Random.Range(0f, 0.7f);
                    clip.freqRamp = -0.2f - Random.Range(0f, 0.2f);
                }
                clip.baseFreq *= clip.baseFreq;
                if (Random.Range(0, 4) == 0)
                    clip.freqRamp = 0.0f;
                if (Random.Range(0, 2) == 0)
                    clip.repeatSpeed = 0.3f + Random.Range(0f, 0.5f);
                clip.envAttack = 0.0f;
                clip.envSustain = 0.1f + Random.Range(0f, 0.3f);
                clip.envDecay = Random.Range(0f, 0.5f);
                if (Random.Range(0, 1) == 0)
                {
                    clip.phaOffset = -0.3f + Random.Range(0f, 0.9f);
                    clip.phaRamp = -Random.Range(0f, 0.3f);
                }
                clip.envPunch = 0.2f + Random.Range(0f, 0.6f);
                if (Random.Range(0, 1) == 1)
                {
                    clip.vibStrength = Random.Range(0f, 0.7f);
                    clip.vibSpeed = Random.Range(0f, 0.6f);
                }
                if (Random.Range(0, 2) == 0)
                {
                    clip.arpSpeed = 0.6f + Random.Range(0f, 0.3f);
                    clip.arpMod = 0.8f - Random.Range(0f, 1.6f);
                }
                break;
            case Presets.POWERUP:
                if (Random.Range(0, 1) == 1)
                    clip.waveType = UnityFX.WaveTypes.SAWTOOTH;
                else
                    clip.duty = Random.Range(0f, 0.6f);
                if (Random.Range(0, 1) == 1)
                {
                    clip.baseFreq = 0.2f + Random.Range(0f, 0.3f);
                    clip.freqRamp = 0.1f + Random.Range(0f, 0.4f);
                    clip.repeatSpeed = 0.4f + Random.Range(0f, 0.4f);
                }
                else
                {
                    clip.baseFreq = 0.2f + Random.Range(0f, 0.3f);
                    clip.freqRamp = 0.05f + Random.Range(0f, 0.2f);
                    if (Random.Range(0, 1) == 1)
                    {
                        clip.vibStrength = Random.Range(0f, 0.7f);
                        clip.vibSpeed = Random.Range(0f, 0.6f);
                    }
                }
                clip.envAttack = 0.0f;
                clip.envSustain = Random.Range(0f, 0.4f);
                clip.envDecay = 0.1f + Random.Range(0f, 0.4f);
                break;
            case Presets.HIT_HURT:
                clip.waveType = (UnityFX.WaveTypes)Random.Range(0, 2);
                if (clip.waveType == UnityFX.WaveTypes.SINE)
                    clip.waveType = UnityFX.WaveTypes.NOISE;
                if (clip.waveType == UnityFX.WaveTypes.SQAURE)
                    clip.duty = Random.Range(0f, 0.6f);
                clip.baseFreq = 0.2f + Random.Range(0f, 0.6f);
                clip.freqRamp = -0.3f - Random.Range(0f, 0.4f);
                clip.envAttack = 0.0f;
                clip.envSustain = Random.Range(0f, 0.1f);
                clip.envDecay = 0.1f + Random.Range(0f, 0.2f);
                if (Random.Range(0, 1) == 1)
                    clip.hpfFreq = Random.Range(0f, 0.3f);
                break;
            case Presets.JUMP:
                clip.waveType = 0;
                clip.duty = Random.Range(0f, 0.6f);
                clip.baseFreq = 0.3f + Random.Range(0f, 0.3f);
                clip.freqRamp = 0.1f + Random.Range(0f, 0.2f);
                clip.envAttack = 0.0f;
                clip.envSustain = 0.1f + Random.Range(0f, 0.3f);
                clip.envDecay = 0.1f + Random.Range(0f, 0.2f);
                if (Random.Range(0, 1) == 1)
                    clip.hpfFreq = Random.Range(0f, 0.3f);
                if (Random.Range(0, 1) == 1)
                    clip.lpfFreq = 1.0f - Random.Range(0f, 0.6f);
                break;
            case Presets.BLIP_SELECT:
                clip.waveType = (UnityFX.WaveTypes)Random.Range(0, 1);
                if (clip.waveType == 0)
                    clip.duty = Random.Range(0f, 0.6f);
                clip.baseFreq = 0.2f + Random.Range(0f, 0.4f);
                clip.envAttack = 0.0f;
                clip.envSustain = 0.1f + Random.Range(0f, 0.1f);
                clip.envDecay = Random.Range(0f, 0.2f);
                clip.hpfFreq = 0.1f;
                break;
        }
    }
}
