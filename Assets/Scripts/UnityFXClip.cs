﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnityFXClip : ScriptableObject
{
    public UnityFX.WaveTypes waveType;

    public float baseFreq;
    public float freqLimit;
    public float freqRamp;
    public float freqDramp;
    public float duty;
    public float dutyRamp;

    public float vibStrength;
    public float vibSpeed;
    public float vibDelay;

    public float envAttack;
    public float envSustain;
    public float envDecay;
    public float envPunch;

    public float lpfResonance;
    public float lpfFreq;
    public float lpfRamp;
    public float hpfFreq;
    public float hpfRamp;

    public float phaOffset;
    public float phaRamp;

    public float repeatSpeed;

    public float arpSpeed;
    public float arpMod;

    public UnityFXClip()
    {
        this.Reset();
    }

    public void Reset()
    {
        waveType = UnityFX.WaveTypes.SQAURE;

        baseFreq = 0.3f;
        freqLimit = 0f;
        freqRamp = 0f;
        freqDramp = 0f;
        duty = 0f;
        dutyRamp = 0f;

        vibStrength = 0f;
        vibSpeed = 0f;
        vibDelay = 0f;

        envAttack = 0f;
        envSustain = 0.3f;
        envDecay = 0.4f;
        envPunch = 0f;

        lpfResonance = 0f;
        lpfFreq = 1f;
        lpfRamp = 0f;
        hpfFreq = 0f;
        hpfRamp = 0f;

        phaOffset = 0f;
        phaRamp = 0f;

        repeatSpeed = 0f;

        arpSpeed = 0f;
        arpMod = 0f;
    }
}